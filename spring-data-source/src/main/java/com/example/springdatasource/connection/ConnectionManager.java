package com.example.springdatasource.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConnectionManager {

    private static Connection connection;

    // 1 kere java vm
    static {
        try {
            String googleMySqlServerIp = "35.232.183.18";
            String port = "3306";
            String schemaName = "/trendyoldb";
            String url = "jdbc:mysql://" + googleMySqlServerIp + ":" + port + schemaName;
            connection = DriverManager.getConnection(url, "root", "csd1993");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return connection;
    }
}
