package com.example.springdatasource;

import com.example.springdatasource.connection.ConnectionManager;
import com.example.springdatasource.domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@SpringBootApplication
@RestController
public class SpringDataSourceApplication {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(SpringDataSourceApplication.class, args);
    }

    @GetMapping("foo")
    public String foo() throws SQLException {

        // önce connection alınır
        // String sql = "INSERT INTO customers (CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES ('XSpring', 'çelik', 'maden mahallesi', 'çanakkale', 'x@gmail.com', '19291')";
        // jdbcTemplate.execute(sql);

        // ...

        List<Category> categoryList = jdbcTemplate.query("select * from categories", new MyRowMapper());
        System.out.println(categoryList);

        Connection connection = ConnectionManager.getConnection();
        String sqly = "INSERT INTO customers (CustomerName, CustomerSurname, CustomerAdress, CustomerCity, CustomerEmail, CustomerPassword) VALUES ('XSpring', 'çelik', 'maden mahallesi', 'çanakkale', 'y@gmail.com', '19291')";
        connection.prepareStatement(sqly).execute();

        return "foo";
    }

}


// deseri, parser, mapper
class MyRowMapper implements RowMapper<Category> {

    @Override
    public Category mapRow(ResultSet resultSet, int i) throws SQLException {
        Category result = new Category();

        // satırı deseri ediyoruz
        int categoryId = resultSet.getInt("CategoryId");
        String categoryName = resultSet.getString("CategoryName");
        String description = resultSet.getString("Description");

        result.setCategoryID(categoryId);
        result.setCategoryName(categoryName);
        result.setDescription(description);

        return result;
    }
}