package com.bugra.githubapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubApiApplication {

    public static void main(String[] args) throws JsonProcessingException {

        int val = 66;
        char A = 'A';
        System.out.println(A);


        // SpringApplication.run(GithubApiApplication.class, args);
        System.out.println("foo");

        // serialization - deserialization - dönüşmek
        String str = "BABA";
        String strJson = "{ \"str\": \"BABA\"}";
        String araba = "\"a\"raba";
        System.out.println(araba);
        System.out.println(strJson);



        String baba = "BABA";

        byte [] babaBytes = new byte[4];
        babaBytes[0] = 66;
        babaBytes[1] = 65;
        babaBytes[2] = 66;
        babaBytes[3] = 65;

        char s = (char) babaBytes[0];
        System.out.println(s);
        System.out.println(babaBytes[0]);


        // 1 tane customer alınır customer modelinin
        Customer c1 = new Customer(2, "bugra");

        // stringe seri ettin dönüştürdün
        String stringVersionCustomer = c1.toString();

        // byte en son byte  // serialization - modelden bytea , customer byte, string byte
        byte[] bytesOfCustomer = stringVersionCustomer.getBytes();

        // tam tersi yani elinde byte array byte array üste doğru
        System.out.println(new String(bytesOfCustomer));

        // string ifadeden nesneye dönüş deseri
        String svar = c1.toString();
        Customer customer = CustomerParse.deseri(svar);
        System.out.println(customer.getId());


        //
        // 1 tane customer alınır
        Customer c2 = new Customer(1, "t");

        // object mapper
        ObjectMapper objectMapper = new ObjectMapper(); // nesne alınır

        // seri ediceksen write metodlarına çağrı
        String customerfoJson = objectMapper.writeValueAsString(c2); // seri etmece

        System.out.println("***");
        System.out.println(customerfoJson);

        Customer customer1 = objectMapper.readValue(customerfoJson, Customer.class);
        System.out.println(customer1.getId());

        Customer x = objectMapper.readValue("{\"id\":1,\"name:\"t\"}", Customer.class);

    }
}


class Customer {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name; // nerede data

    public Customer() {
    }

    public Customer(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}